package com.jhonystein.pedidex.utils;

public interface EnumType {
    String getKey();
}
