package com.jhonystein.pedidex.rest.resources;

import com.jhonystein.pedidex.model.Cliente;
import com.jhonystein.pedidex.rest.AbstractCrudResource;
import com.jhonystein.pedidex.services.AbstractCrudService;
import com.jhonystein.pedidex.services.ClienteService;
import com.jhonystein.pedidex.utils.auth.RequestAuth;
import javax.inject.Inject;
import javax.ws.rs.Path;

@RequestAuth
@Path("clientes")
public class ClienteResource extends AbstractCrudResource<Cliente> {

    @Inject
    private ClienteService service;

    @Override
    protected AbstractCrudService<Cliente> getService() {
        return service;
    }

}
