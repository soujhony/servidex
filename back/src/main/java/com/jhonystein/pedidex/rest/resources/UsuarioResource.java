package com.jhonystein.pedidex.rest.resources;

import com.jhonystein.pedidex.model.Sessao;
import com.jhonystein.pedidex.model.Usuario;
import com.jhonystein.pedidex.services.UsuarioService;
import com.jhonystein.pedidex.utils.auth.Logged;
import com.jhonystein.pedidex.utils.auth.RequestAuth;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("auth")
@RequestScoped
public class UsuarioResource {

    @Inject
    private UsuarioService service;
    
    @Inject @Logged
    private Sessao sessaoUsuario;
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(Usuario usuario) {
        Sessao sessao = service.login(usuario);
        if (sessao == null) 
            return Response.status(Response.Status.UNAUTHORIZED).build();
        return Response.ok(sessao).build();
    }
    
    @GET
    @Path("@me")
    @RequestAuth
    @Produces(MediaType.APPLICATION_JSON)
    public Sessao me() {
        return sessaoUsuario;
    }
    
    @POST
    @Path("registro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Sessao registro(Usuario usuario) {
        return service.login(service.insert(usuario));
    }
}
